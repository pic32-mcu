                   PIC32 Example Projects Using Mikropascal

PIC32 Microcontrollers

   Unlike the 8-bit [1]PIC microcontroller family, the [2]Microchip 32-bit
   [3]PIC32 microcontrollers, which are based on the [4]MIPS architecture,
   are pretty delightful to use. Unfortunately I find the [5]Microchip
   development software not delightful. It is expensive, bloated, and hard
   to use.

   Microchip sells some interesting PIC32 microcontrollers in DIP
   packages, including some with USB device interfaces. Such devices are
   easy to prototype with using solderless breadboards, if you can stand
   to write the software for them. They are also very easy to buy in small
   quantities from [6]Microchip Direct.

mikroPascal for PIC32

   The [7]Mikroelektronika [8]mikroPascal for PIC32 Pascal compiler makes
   working with PIC32 devices very easy. The compiler hides almost all of
   the details of the PIC32 architecture. Getting the oscillator and PLL
   configuration correct can require some trial and error, though.

   mikroPascal has a very extensive [9]software component library for the
   PIC32, ranging from trigonometry functions to USB HID device support. I
   am convinced that the true value of a microcontroller development
   environment lies in the richness of its software component libraries
   more than any other factor. In this regard, mikroPascal for PIC32
   excels and is worth every penny of its $299 USD price (which includes
   lifetime updates).
   _______________________________________________________________________

   Questions or comments to Philip Munts [10]phil@munts.net

   I am available for custom system development (hardware and software) of
   products using PIC32 or other microcontrollers.

References

   1. http://www.microchip.com/design-centers/8-bit/pic-mcus
   2. http://www.microchip.com/
   3. http://www.microchip.com/design-centers/32-bit
   4. https://en.wikipedia.org/wiki/MIPS_instruction_set
   5. http://www.microchip.com/mplab/mplab-x-ide
   6. https://www.microchipdirect.com/
   7. http://www.mikroe.com/
   8. http://www.mikroe.com/mikropascal/pic32
   9. http://www.mikroe.com/mikropascal/pic32/libraries
  10. mailto:phil@munts.net
